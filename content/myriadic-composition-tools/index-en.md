Title: Myriadic Composition Tools
Lang: en
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou


If software is a functional object, it is also a cultural object that embodies its creators vision about the way things ought to be done. Moreover, this vision is always inscribed in a lineage of preceding tools and practices. 

When one launches a layout software program, one starts a dialogue with 50 years of modern computing history, and with more than 500 years of printing history, extending into the history of writing and of the sign. Even if modernist gurus such as Jan Tschichold or Joost Hochuli have preached universal composition rules, the practice of design shows that these can never be valid for each and every type of book, culture, or content even. Details such as how to hyphenate, or how text blocks are constructed are all results of cultural and context specific decisions. The texts in this section then all reflect on this question: how do technical and social aspects influence and give meaning to one’s way to layout and typeset?

Maurice Girod’s [*Ordinateur et composition automatique*](ordinateur-et-composition-automatique.html) explains how automatic text composition (through hyphenation and justification) cannot be valid for every language, not even for publishers writing in the same language.

While Girod speaks of text justification through technology and language, Robin Kinross, in [*Unjustified text and the zero hour*](unjustified-text-and-the-zero-hour.html), traces back its history through technological innovation and social contexts with a cultural approach.

Alexandre Leray and Femke Snelting’s text [*Books with an Attitude*](books-with-an-attitude.html) speaks about the design of one content for multiple outputs. It criticises the impoverishment which results from the decision to aim for a correct display which matches the lowest common denominator. Instead of adopting an approach which aims at eliminating the differences accross devices and produce a seamless experience, the authors call for a design which considers specificity as an opportunity for "graceful optimisation".


Content
-------

* [*Ordinateur et composition automatique*](ordinateur-et-composition-automatique.html)
* [*Unjustified text and the zero hour*](unjustified-text-and-the-zero-hour.html)
* [*Books with an Attitude*](books-with-an-attitude.html)
* [*TeX and daughters: an interview with Jacques André*]() (working title)
