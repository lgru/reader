Title: Considering your tools
Lang: en
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

> «Approaching a practice then means approaching it as it diverges, that is, feeling its borders, experimenting with the questions which practitioners may accept as relevant, even if they are not their own questions, rather than posing insulting questions that would lead them to mobilise and transform the border into a defence against their outside.»
> 
> Isabelle Stengers, «Introductory Notes on an Ecology of Practices»

Today’s creation largely depends on digital tools. Far from being a neutral means to an artistic achievement, those tools are actually opinionated: they carry values and are full of conventions about the way things “ought” to be done.  
To us, a greater awareness of the role of digital tools is—if important to everybody—crucial in the education of artists and designers. Instead of means, the soft- and hardware tools can become partners to consciously think and converse with, to question and interrogate and to clash with. And because (visual) creation is so tightly coupled with technological development, a larger awareness of these tools can help one speculate about future practices and invent the tools to support them.

Contrary to other contemporary fields of creation, there is little literature on these questions in the sphere of graphic design. This is why we felt it was important to bring together texts and showcases on this topic into one comprehensive corpus: a tool to think about tools.

With only five exceptions, all materials in this reader is available under licenses that invite re-use, distribution and re-appropriation. This means that texts can circulate freely, and be included in other digital and printed publications; can be translated and otherwise used as necessary.

Content
=======

This publication contains newly commissioned, translated and re-issued texts, distributed over 5 chapters:

* [Discrete gestures](discrete-gestures) is about how our body is informed by our digital tools.
* [Reading interfaces](reading-interfaces) takes on different approaches to computer literacy.
* [The making of the standards](the-making-of-the-standards) is about the social and technical processes behind the elaboration of norms.
* [Myriadic composition tools](myriadic-composition-tools) tackles the question of software as a cultural object through the lens of digital typography.
* Finally, [Shaping processes](shaping-processes) discusses methodologies for open and critical collective practices. For each chapter, a short note problematize the questions behind the texts.
* A text by Belgian philosopher Isabelle Stengers and a general introduction give a broader picture on the question of tools and free culture in relation to practices.
