Type: article
Authors: Henri de Montrond
Title: Un code de la mise en pages
Date: 1976
Journal: Communication et langages
Pages: 39-48
Original: http://www.persee.fr/web/revues/home/prescript/article/colan_0336-1500_1981_num_49_1_1441
License: CC-BY-NC-ND
Lang: fr

Il existe déjà un code typographique...
=======================================

Avant d'aborder ce code de mise en pages, il faut rappeler
qu'il prend la suite d'un autre code auquel nous nous
soumettons totalement [^1].

Il existait jusqu'à ce jour un code typographique sur lequel
l'ensemble de la profession de l'imprimé se trouve en parfait
accord. Le caractère traditionaliste de cette profession en a
certainement facilité la transmission. Dans toute imprimerie de
presse ou de labeur, il se trouvera toujours un chef d'atelier
(on l'appelait jadis le prote) pour défendre les principes sacro-saints 
du code typographique. Et ce que nous disons du prote,
souvent nous pouvons l'affirmer de la plupart des typographes
qui ont acquis cette discipline du bien-écrire en même temps
qu'ils s'exerçaient aux gestes professionnels.

Cependant, ces dernières années, une désaffection semblait
se dessiner à l'égard du code et cela pour des raisons différentes : 
l'arrivée dans la profession de nouveaux venus plus
esthètes que grammairiens — nous en reparlerons par la suite.
Ils apparurent au typographe comme des trouble-fête bousculant
les habitudes professionnelles. Celui-ci commença par s'entêter
dans ses principes, mais souvent, de guerre lasse, renonça à
son « catéchisme ».

Une autre nouveauté vint faire échec très indirectement à la
structure d'airain du code typographique : la photocomposition.
Voilà qu'une nouvelle venue en blouse blanche et aux doigts
peints, jeune claviste sortant d'une école de dactylographie,
prenait place dans l'imprimerie, pour ne pas dire qu'elle prenait
la place du typographe en salopette, manches relevées et mains
graisseuses, manieur de plomb mais porteur d'un héritage.
Nous ne voulons pas sous-entendre ici que les jeunes informaticiennes
méprisent la sagesse des traditions linguistiques
de l'imprimeur, mais simplement que leur formation différente
risque de les laisser en deçà de cette science empirique et quasi
amoureuse du disciple de Gutenberg.


... Il n'existe pas de code de mise en pages
============================================

Devant le risque d'une déperdition de la transmission du code
typographique, certains responsables se sont alarmés et c'est
ainsi que le C.P.J.[^2] accueillait une commission d'imprimeurs,
de journalistes, de graphistes et de fondeurs pour simplifier
le code, espérant ainsi ne pas trop décourager les nouvelles
recrues devant l'apparente jungle que représente le code pour
un débutant.

Il avait été précédé par l’École de Lure, association de graphistes
que l'on connaît pour son souci des problèmes de la typographie.
On y décidait en 1975 de recevoir un atelier qui aurait
pour tâche la simplification éventuelle du code. Or les discussions
préparatoires à cette démarche firent apparaître une
nécessité plus grande encore et plus immédiate, celle d'acquérir
un même langage dans le domaine de la mise en pages, et
c'est ainsi qu'abandonnant à d'autres la poursuite du premier
objet ils décidèrent une enquête dont nous allons vous retracer
les étapes.

Un nouveau-né: le graphiste
===========================

Nous ne voulons pas développer ici le phénomène pourtant fort
intéressant de la prolifération des exigences graphiques ces
trente dernières années : c'est un autre propos. Nous signalons
simplement l'« intrusion » à l'imprimerie de ce nouveau partenaire, 
le graphiste fanatique du visuel, qui véhicule avec lui
quelquefois l'insolence de ses convictions. Dans ses relations
avec les hommes du plomb, il acquerra souvent, de façon empirique,
les bribes d'un jargon dont il ignore encore les subtilités.

Et, quand nous parlons de jargon, c'est sans aucun mépris, nous
pourrions aussi bien dire « langue professionnelle », tant il est
vrai que chaque vrai métier véhicule avec lui un langage propre,
souvent séculaire, réservé aux seuls initiés.

Ce même graphiste fréquente simultanément une autre officine
qui s'appelle photogravure, industrie plus récente, née, son nom
l'indique, d'une autre découverte presque aussi révolutionnaire
que celle de Gutenberg ; autres techniques, autre jargon, mais
celui-ci moins unanime et souvent très fantaisiste.


Des spécialistes s'interrogent
==============================

Tout cela explique que, lorsqu'à Lurs, en Provence, au pied de
la montagne de Lure, sous les amandiers, au cours de colloques
vespéraux, quelques dizaines de graphistes rassemblés évoquèrent
les termes respectifs de leur langage professionnel,
l'on put se croire pour quelque temps à Babel : à la stupeur
générale, chacun s'aperçut que la même consigne pouvait être
donnée en des termes totalement différents d'où, l'on s'en doute,
le risque de confusions, aux conséquences parfois graves, dans
une industrie comme celle de l'imprimerie.

Se trouvaient là rassemblés des spécialistes de plusieurs
nationalités : Français, Canadiens, Suisses, Italiens, Belges,
Anglais ; tous furent intimement persuadés de la nécessité
d'un ajustage collectif d'une langue professionnelle qui était
encore en gestation : ils venaient de décider la création d'un
code de mise en pages.

Il est bien entendu, écartons dès maintenant toute équivoque,
qu'il ne s'agissait aucunement de définir une méthode quelconque
d'architecture de la page, mais de dresser une liste
aussi exhaustive que possible des consignes que le graphiste
doit transmettre simultanément aux imprimeurs et aux photograveurs.

Cette liste devait répondre aux gestes les plus usuels, les plus
urgents (viendraient ensuite les nuances). Voici la première
liste telle qu'elle fut dressée après une semaine de confrontations : 
composition normale d'un texte en lignes égales ;
composition d'un texte en lignes inégales, celles-ci étant alignées
sur la gauche ou sur la droite ; composition d'un texte en
lignes inégales mais centrées. Voilà pour les consignes à transmettre
à l'imprimeur.


Comment signifier les consignes ?
=================================

Lorsqu'on sait que de telles consignes doivent souvent être
exécutées dans les minutes qui suivent, s'il s'agit, par exemple,
de la presse quotidienne, qu'elles peuvent être données par
téléphone dans des cas d'extrême urgence ou enfin, quelquefois,
transmises par-delà les frontières, étant donné, depuis quelques
années, la multiplicité des échanges internationaux, on n'eut
pas de peine à conclure qu'au-delà du vocabulaire il était très
souhaitable de se mettre d'accord sur des signes.

Un signe se doit évidemment d'être d'un tracé simple et rapide,
sans équivoque, ne se rapprochant donc pas des formulations
déjà existantes, et — puisqu'il s'agit souvent, en la circonstance,
d'aller vite et de gagner du temps — de découler d'un geste de
plume ou de feutre.

Quatre signes furent donc proposés dans un formulaire qui
devait être distribué dans la profession au cours de l'année
1975-1976. Il en fut de même pour cinq autres signes portant sur
les opérations de photogravure. Chacun de ces signes portait
un qualificatif également proposé parmi ceux qui avaient paru
les plus usuels, disons les plus fréquents.


Dans le désordre
================

Le propre de la démarche menée par les Compagnons de l’École
de Lure était de dégager des constantes afin de proposer à
l'usage collectif sinon les formules les plus usitées, au moins
les plus logiques. Or il a paru, en fin de compte, que le bon sens
est peut-être finalement « la chose du monde la mieux partagée ». 
En effet, les propositions, résultat d'une démarche en
milieu fermé, furent, pour certaines, contredites dans les
réponses de l'enquête professionnelle plus exhaustive[^3].
Il est peut-être nécessaire de commenter quelque peu, les uns
à la suite des autres, les signes que nous proposons plus bas.


Texte en pavé
-------------

![(IMG)][PAVE1]

1. La colonne de texte traditionnellement
utilisée dans nos journaux ou dans nos
livres (ici même) pourrait, à la rigueur,
se passer d'une formulation, tant elle
est courante et d'un usage habituel. Le
terme le plus courant (nous ne disons
pas le seul, loin de là) est le pavé.

La copie sera annotée par le signe
multiplié par (y) suivi de la justification
souhaitée (en termes profanes, la longueur
de la ligne traduite en mesure
typographique)

Le graphiste sur sa maquette, l'exprimera
par une flèche bicéphale délimitant le début 
et la fin de la ligne, comme le
ferait un menuisier pour indiquer la hauteur 
et la largeur d'une pièce de bois.
À noter, c'est un détail qui a son importance, 
que la pointe des deux flèches
sera tracée comme un petit triangle et
non remplie pour ne faire illusion en
aucune façon avec telle flèche réelle,
décorative ou explicative, qui pourrait
intervenir dans un texte.

![(IMG)][PAVE1]

Pour ceux que cela pourrait intéresser,
qu'ils sachent que l'amplitude de la
colonne de texte ainsi amorcée, colonne
de journal ou paragraphe d'article, sera
signifiée par le graphiste à l'imprimeur
par un méandre rapidement tracé à la
main, qui part de la première ligne verticalement
pour descendre jusqu'à son
point extrême, calculé par le graphiste
ou à déterminer, dans certaines conditions, 
par l'imprimeur selon leur accord ;
dans ce cas, l'extrémité de la flèche
s'arrêtera en cours de route, suivie de
trois points de suspension.

Lignes inégales
---------------

![(IMG)][INEGALES]

2. La colonne de texte bien équerrée à
gauche, se terminant en dents de scie
sur la droite : nous évoquons ici un mode
de composition usité plutôt dans les
pages de publicité. Soit dit en passant,
ce mode de composition, s'il est peu
habituel dans l'imprimé français, est
d'ores et déjà courant dans les revues
Scandinaves, par exemple ; il a le mérite
de dresser des colonnes de texte beau
coup moins monolithiques en regard
d'illustrations souvent équerrées elles-
mêmes à outrance, mais ce n'est pas
notre propos.

Le signe proposé, qui figurera aussi bien
sur le texte manuscrit, préparé à partir
pour l'imprimerie, que sur la maquette,
sera une flèche appuyée sur la verticale
de l'alignement, à droite ou à gauche,
selon les cas, et partant vers la gauche
ou vers la droite respectivement (voir
croquis ci-contre).

Il est intéressant de constater que c'est sur ce point que l'unanimité a eu le plus de mal à se faire. La loi du grand nombre l'a emporté qui manifestait une logique plus gestuelle que verbale, ce qui est normal pour des graphistes, le qualificatif retenu étant : 
*aligné à gauche*, ou :
*aligné à droite*, ce qui paraît couler de
source, mais qui fit couler beaucoup
d'encre[^4].

Lignes centrées
---------------

![(IMG)][CENTREES]

3. Reste à définir, à signifier la colonne
de texte symétriquement découpée sur
|a gauche et sur la droite, forme de
composition plus courante. Nous parlerons de lignes centrées, et le graphiste,
sur le manuscrit qu'il prépare pour
primeur, utilisera une flèche bicéphale
pointée en son milieu. Sur sa maquette,
il tracera le même signe, mais qui, cette
fois, ne jouxtera pas les deux extrémités
maximales de son gabarit (voir ci-contre).
Il est à remarquer que cette composition
trouva sans difficulté sa formulation et
presque à l'unanimité[^5].

Avant de conclure, nous allons tout de
même passer en revue quelques autres
signes (ils font partie eux aussi du code
de mise en pages) qui s'adressent aux
photograveurs.

Ils figureront souvent sur les mêmes
maquettes que les précédents, car nombre d'imprimeurs sont actuellement 
équipés en photogravure et, de toute façon,
au sein du journal, pour longtemps encore,
tout se passera en un même lieu.

C'est d'ailleurs à des journalistes[^6] que
nous en devons la primeur: ils ont été
les premiers à ressentir la nécessité de
cette formulation d'un code.

Il est fort possible que les usagers soient
quelque peu déçus par leur graphisme
manquant de simplicité. Qu'ils s'inte
rrogent eux-mêmes et qu'ils sachent enfin
que plusieurs années de recherches col
lectives
ont donné le résultat que voici :

Codification des titres
=======================

![(IMG)][CODIFICATION1]

1. Une lettre blanche doit apparaître sur
un fond noir ou sur un fond de couleur :
nous parlerons d'une lettre, d'un titre *en
réserve*, et le signe proposé sera un
carré blanc dans un carré noir (voir ci-
contre). Le terme « en réserve », qui, lui
aussi, connaît beaucoup de synonymes,
est le moins équivoque, et il facilite les
explications subtiles. On parlera d'un titre
en réserve dans le noir, mais aussi bien
d'une réserve dans la couleur ; on pourra
même préciser en quadrichromie. Un
exemple : titre en réserve dans le rouge
et dans le jaune si on le souhaite bleu,
etc.

![(IMG)][CODIFICATION2]

2. Ce même titre doit figurer sur un fond
gris : nous parlerons d'une *réserve sur
trame*, et ce terme de trame sera toujours
et fatalement accompagné du pourcentage
correspondant au gris que l'on
veut obtenir (à la couleur descendue s'il
s'agit de couleur). Cela peut apparaître
comme une lapalissade ; cependant, il
est important d'insister sur le fait que
le pourcentage correspondra désormais
au résultat imprimé que l'on souhaite.
Pour cette opération, le graphisme sera
un petit carré blanc apparaissant sur des
hachures (voir ci-contre).

Les lecteurs qui seraient surpris de notre
insistance ignorent les malentendus qui
peuvent naître des opérations de photo
gravure
passant par un négatif avant
d'obtenir le positif et réciproquement.

![(IMG)][CODIFICATION3]

3. Le titre doit apparaître en noir sur un
fond gris ou sur un fond de couleur atténué. La formulation, cette fois, est sans
équivoque : *trait sur trame*, et son graphisme : un carré noir sur un hachuré
(voir ci-contre).

Pourquoi trait ? Cette expression, qui
paraît agréée de tous, est réservée à
toute forme de cliché non tramé.

Nous nous excusons auprès des lecteurs
étrangers à ces techniques de photo
gravure,
ils risquent tout à coup, faute
d'explications suffisantes, de perdre le fil
de notre propos. Ils comprendront que
nous ne pouvons pas ici développer un
exposé sur la photogravure, mais nous
en avons presque fini avec ces subtilités
professionnelles.

![(IMG)][IMAGES]

Qu'il nous soit permis d'évoquer très
rapidement les deux derniers signes du
code dans leur état actuel : le cliché trait,
s'il faut le distinguer plus particulière
ment,
sera indiqué sur la maquette par
une transversale, alors que le cliché
tramé (simili, tel est son nom) sera indiqué par l'entrecroisement de deux diagonales.


Comme nos lecteurs l'ont compris, la définition de ce code de
mise en pages a été le fruit d'une recherche collective. Si l'on
ne peut encore parler d'un consensus absolu, il faut toutefois
admettre que la logique et le bon sens ont été les perpétuels
points de référence d'abord des enquêteurs (ils furent tous des
membres de la profession : imprimeurs, photograveurs, journalistes et graphistes), des rédacteurs enfin, réunis de nouveau en
session internationale aux colloques annuels de l’École de Lure.


Ce code de mise en pages s'adresse pour le moment aux pays
de langue française. Dans un avenir que nous espérons pas
trop lointain, il se confrontera avec les propositions des pays
de langue anglaise et allemande au sein de l'association inter
nationale
qu'est l'Atypi. En effet, il est intéressant de constater
qu'au moment où démarrait en France cette enquête, en Angleterre et en Pologne des initiatives du même genre prenaient le
départ.

Il est certain que, dans un premier temps, les familiers de
l'imprimerie et de la photogravure qui ne retrouveront pas
exactement le recueil de leurs habitudes marqueront une certaine
réticence à l'utilisation de ces signes, mais il est fort
probable qu'à la longue, la nécessité l'emportant, beaucoup
d'entre eux se rallieront à cet usage.

Nous sommes persuadés qu'avant même que cette génération
ne transmette son outil à celle de ses fils elle se sera familiarisée
avec un langage que ceux-ci auront appris à l'école.
S'il est un univers que nous n'avons pas évoqué : celui des
informaticiens, c'est parce que nous savons que leurs problèmes
sont d un ordre particulier ; leurs instruments respectifs ont
leur propre formulation, mais, sur les lieux où nous sommes
appelés à nous rencontrer — cela aussi, nous le savons d'expérience — , ils ont toujours souhaité ajuster les longueurs d'onde
et parler le langage de leurs « clients ».

Pour conclure, il faut évoquer une réflexion qui paraphait la
réponse d'un graphiste à l'enquête ; celui-ci résumait en quelque
sorte l'assentiment unanime des autres : « Le langage significatif
de ce code facilitera considérablement nos rapports professionnels, c'est évident, mais il ne remplacera jamais l'"apprivoisement" nécessaire entre deux partenaires qui souhaitent devenir des collaborateurs. »

![(IMG)][TABLEAU]


[^1]: Nous ne rappellerons pas ici les signes typographiques traditionnellement
reçus et que tous les usagers sont censés connaître. Toutefois, il est peut-
être utile de signaler que l'Ignia a édité un parfait instrument : *Préparation
de la copie et Correction des épreuves*, de Damier Auger (Paris, coll. « Espace », 
140, rue de Rivoli).

[^2]: C.P.J. : Centre de perfectionnement des journalistes.

[^3]: Ici, nous introduirons une petite parenthèse quant aux résultats de l'enquête canadienne qui fut parallèlement menée. Que nos amis québécois ne
nous en veuillent pas pour cette remarque : est-ce, chez eux, la manifestation d'un sens inné de la discipline ? À 90 %, leurs réponses furent un
acquiescement sans réserve qui les met actuellement en contradiction avec
le code définitif.

[^4]: En effet, avant d'en arriver à cette simplicité,
ou même à cette lapalissade, il fallut énumérer,
analyser, soupeser puis écarter de nombreuses
expressions dont certaines très fantaisistes. Pour
donner une idée de la fantaisie de vocabulaire
qui sévit dans la profession, nous ne citerons ici
que quelques synonymes usités : ligne brisée, au
fer, en dents de scie, en sommaire brisé, au fer
américain, lignes inégales, bouzillé droite ou
gauche, en drapeau. Les usagers de la profession
ne nous contrediront pas.    
Qu'ils soient assez modestes pour accepter en
la circonstance la loi du grand nombre, nous
finirons par éviter ainsi une foule de malentendus
souvent constatés lorsqu'il est trop tard,

[^5]: Elle est usitée depuis les origines, dans le
titrage du livre en particulier.

[^6]: C'est à l’École de journalisme de Lille, et en
particulier à l'un de ses éminents professeurs,
Pierre Laugié, que nous devons l'ébauche de ces
signes. Il est secondé à la « Croix du Nord » par
Claude Corvisart qui fut l'un des premiers artisans de l'enquête.


[PAVE1]: /media/images/texte-en-pave-web.gif
[PAVE2]: /media/images/texte-en-pave-2-web.gif
[INEGALES]: /media/images/lignes-inegales-web.gif
[CENTREES]: /media/images/lignes-centrees-web.gif
[CODIFICATION1]: /media/images/codification-des-titres-1-web.gif
[CODIFICATION2]: /media/images/codification-des-titres-2-web.gif
[CODIFICATION3]: /media/images/codification-des-titres-3-web.gif
[IMAGES]: /media/images/images-web.gif
[TABLEAU]: /media/images/tableau-web.gif
