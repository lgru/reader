Title: La fabrication des standards
Lang: fr
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

> "Pour ceux qui travaillent dur et longtemps pour élaborer des normes bonnes et justes, ainsi que pour ceux qui ont souffert de leur absence. D'une part, la lutte contre la tyrannie de l'absence de structure. D'autre part, l'illusion d'une taille unique."
> 
> — Lampland, Martha, and Susan Leigh Star. 2009. Standards and their stories: how  quantifying, classifying, and  formalizing practices shape everyday life. Ithaca: Cornell University Press.

Fabriquer un standard n'est pas seulement une tâche technique. C'est un processus social dans lequel pouvoir, négociation, contraintes et compromis entrent en jeu. Les stadards à la fois discriminent et affinent, incluent et excluent. Qu'ils soient _de facto_ ou négociés, comment les standards affectent-ils les designers graphiques? Qu'est ce qui fait qu'un standard réussi ou non? Ce chapitre contient trois ensembles de textes qui adressent ces questions.

Le premier ensemble (Jacquerye, Unicode Inc.) parle d'Unicdoe, la norme unifiée ayant pour but de décrire tous les caractères en utilisation sur terre. Ces textes discutent de la hierarchie implicite entre languages centraux et languages périphériques, et comment la structure même de la classification Unicode reflète une géopolitique établie. En même temps Jacquerie présente des projects typographiques collectifs essayant de compenser ce déséquilibre, à l'intérieur de ce système, et d'honorer la richesse des languages du monde.

Le deuxième ensemble (Froshaug, Open Source Publishing, de Montrond) examine les règles implicites et explicites qui régulent les designers et les imprimeurs. Ces textes évaluent le poids de l'infrastructure de l'imprimé et la relation entre qualité et accès, ainsi que l'importance d'un vocabulaire commun entre les différentes parties impliquées.

De points de vue différents mais convergeants, le troisième ensemble (Arnaud, Pilgrim, www-talk, Schrijver) documente comment le language HTML est débatu et défini, influençant la manière dont le web est façonné. Il donne une compte-rendu historique sur la manière dont la balise `<img>` et la spécification HTML5 sont nées et les différentes tensions et conflits gravitant autour des normes négociées pour le Web. Ces contributions insistent de différentes manières sur le role actif joué par les gens qui se préocupent du Web comme espace créatif.

Content
-------

* [Unicodes](unicodes.html)
* [Archive of Notices of Non-Approval]()
* [Sur la typographii](sur-la-typographie.html)
* [Opening the black box of printing](opening-the-black-box-of-printing.html)
* [Un code de la mise en pages](un-code-de-la-mise-en-page.html)
* [W3C go Home! (c’est le HTML qu’on assassine)](w3c-go-home.html)
* [How did we get here ?](how-did-we-get-here.html)
* [Proposed New Tag: IMG](proposed-new-tag-img.html)
* [Who Makes Standards](who-makes-standards.html)
