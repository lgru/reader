Title: Reading Interfaces
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

The division and the implicit hierarchy between the user who accesses the interface and the developer who has insight into the underlying code sparks many questions. How does an interface expose and translate the underlying digital objects?  To what extent does it frame and script the user behaviour? Are coding skills necessary for computer literacy? And is it possible to actively/deeply engage with software, if one confines oneself to the provided interface?

All texts in this chapter focus on the figure of the user, as opposed to putting the task at the centre of the attention. In doing so, they suggest that users should be busy with computing; that is to be active and adopt to what Florian Cramer calls a "writerly" approach to interfaces. Cramer here borrows from Barthes’ distinction between readerly and writerly texts. Not “linear and smoothly composed”, the writerly text reflects the “plurality of entrances, the opening of networks, the infinity of languages” and aims to "make the reader no longer a consumer, but a producer of the text". Even if such an emancipation of the user is a goal shared by the different authors of this chapter, they have different views on what this *writerly* approach consists of.

[For Florian Cramer](echo-echo-echo-echo-command-line-poetics.html), it is necessary to avoid the separation between programmer and user at any cost. Or to put it differently, to be empowered, users should be programmers too. According to him, the graphical user interface provides solutions while the command line provides methods, and he advocates the use of the latter as a means to be always in an active, "writerly" mode.

[Alan Kay](about-alan-kay-a-user-interface-a-personal-view.html) also sees programming as the ultimate stage of computer literacy but for him the Graphical User Interface is not a problem but a means to that end. His text is a plea for a progressive learning curve so that young people especially, who are not yet equipped to think of abstract things, can get familiar with computers.

[Olia Lialina](turing-complete-user.html) shifts focus to the figure of the user. She argues that software production has become so deeply layered that one is always using code programmed by someone else. The author proposes different means to make one's computer personal. For her, computing should not become invisible as the figure of the user might vanish with it. Without it, the possibilities, freedom and rights of users to misuse their software would disappear.

Finally, [Lev Manovitch](inside-photoshop.html) zooms out from the direct relationship between the user and the interface to analyse both the context in which this relationship takes shape and the history from which interfaces borrow their metaphors.

Content
-------

* [(echo echo) echo (echo): Command Line Poetics](echo-echo-echo-echo-command-line-poetics.html)
* [About : Alan Kay, "A User Interface: a Personal View"](about-alan-kay-a-user-interface-a-personal-view.html)
* [Turing complete user](turing-complete-user.html)
* [Inside Photoshop](inside-photoshop.html)
