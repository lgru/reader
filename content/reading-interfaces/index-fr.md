Title: Interfaces de lecture
Lang: fr
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

La division et la hierarchie implicite entre l'utilisateur qui accèdent à l'interface et le dévelopeur qui a accès au code sous-jacent suscite de nombreuses questions. Comme une interface expose et traduit-elle les objets numériques sous-jacents? Jusqu'à quel point encadre t-elle et scénarise t-elle le comportement de l'utilisateur? Des compétences en programmation sont-elles nécéssaires pour le lettrisme numérique? Et est-il possible de s'engage activement et en profondeur avec les logiciel, si l'on s'en tient (se contente, se borne) aux interfaces fournies?

Tous les textes dans ce chapitre se focalisent sur le figure de l'utilisateur, par opposition au fait de mettre la tâche au centre de l'attention. Ce faisant, ils suggèrent que les utilisateurs devraient être préoccupés par l'informatique; c'est à dire être actif et adopter ce que Florian Cramer appelle une approche "scriptible" des interfaces. Cramer emprunte ici la disctinction de Barthes entre les textes "lisibles" et "scriptibles". Non "composés linéaires et régulièrement", les textes "scriptibles" reflètent une "pluralité de points d'entré, l'ouverture de réseaux, l'infinité de languages" at vise à "faire du lecteur non plus un consommateur, mais un producteur du texte"; Même si une telle émancipation de l'utilisateur est un but partagé par les différents auteurs de ce chapitre, ils on différents points de vue sur ce en quoi consiste cette approche "scriptible".

Pour Florian Cramer, il faut éviter à tout prix la séparation entre programmeurs et utilisateur. Ou en d'autres termes, pour être capacités, les utilisateurs devrait être également programmeurs. Selon lui, l'interface graphique utilisateur fournie des solutions alors que la ligne de commande fournie des méthodes, et il plaide pour l'utilisation de cette dernière afin d'être toujours dans une position active, "scriptible".

Alan Kay voit également la programmation comme l'étape ultime du lettrisme informatique mais pour lui l'interface graphique utilisateur n'est pas un problème mais un moyen pour accéder à cette fin. Son texte est un plaidoyer pour une courbe d'apprentisage progressive pour que les jeunes tout particulièrement, qui ne sont pas encore équipés pour penser des choses abstraites, puissent se familiariser avec l'informatique.

Olia Lialina attire l'attention sur la figure de l'utilisateur. Elle avance que la production logicielle est devenue si profondément stratifiée qu'on utilise toujours du code programmé par quelqu'un d'autre. L'auteure propose différentes manières pour s'approprier l'informatique. Pour elle, l'informatique ne devrait pas devenir invisible car sinon la figure de l'utilisateur pourrait disparaître avec. Sans elle, les possibilités, libertés et droits des utilisateurs de mésutiliser leurs logiciels pourrait disparaitre.

Enfin, Lev Manovitch dézoome de la relation directe entre l'utilisateur et l'interface pour analyser à la fois le contexte dans lequel cette relation se forme et l'histoire à partir de laquelle les interfaces empruntent leurs métaphores.


* [(echo echo) echo (echo): Command Line Poetics](echo-echo-echo-echo-command-line-poetics.html)
* [About : Alan Kay, "A User Interface: a Personal View"](about-alan-kay-a-user-interface-a-personal-view.html)
* [Turing complete user](turing-complete-user.html)
* [Inside Photoshop](inside-photoshop.html)
