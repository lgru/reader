# Converts Markdown files to HTML
#
# Usage:
#
#     python generate.py infile.md outfile.html


import codecs
import os
import markdown
import sys

from django.template import Context, loader
from django.conf import settings


PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
settings.configure(
    TEMPLATE_DIRS=(os.path.join(PROJECT_DIR, 'templates'),),
)

template = loader.get_template('text.html')

f = codecs.open(sys.argv[1], "r", encoding="utf-8")
src = f.read()

md = markdown.Markdown(output_format="html5", extensions=['extra', 'meta', 'headerid(level=2)'])
body = md.convert(src)

context = Context({
    'title': 'title',
    'body': body,
    'meta': md.Meta if hasattr(md, 'Meta') else {}
})

f = codecs.open(sys.argv[2], "w", encoding="utf-8")
f.write(template.render(context))
f.close()
